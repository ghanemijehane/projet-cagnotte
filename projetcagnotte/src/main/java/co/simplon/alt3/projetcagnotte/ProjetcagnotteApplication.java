package co.simplon.alt3.projetcagnotte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetcagnotteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetcagnotteApplication.class, args);
	}

}
