package co.simplon.alt3.projetcagnotte.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.projetcagnotte.business.UserBusiness;
import co.simplon.alt3.projetcagnotte.entities.User;
import co.simplon.alt3.projetcagnotte.exceptions.UserExistsException;
import co.simplon.alt3.projetcagnotte.repositories.UserRepo;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserRepo repo;


    @Autowired
    private UserBusiness business;


    //FIND ALL

    @GetMapping
    public List<User> all() {
        return repo.findAll();
    }

    // FIND BY NAME

    @GetMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public User add(@RequestBody @Validated User user) {
        repo.save(user);
        return user;
    }
    // @PostMapping
    // @ResponseStatus(code = HttpStatus.CREATED)
    // public User register(@Valid @RequestBody User user) {

    //     if(repo.findByMail(user.getMail()).isPresent()) {
    //         throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Already Exists");
    //     }

      
    //     user.setRole("ROLE_USER");
    //     repo.save(user);
    //     return user;
    // }


    //INSCRIPTION

    // @PostMapping
    // public User register(@Valid @RequestBody User user) {
    //     try {
    //         return business.register(user);
    //     } catch (UserExistsException e) {
    //         throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
    //     }
    // }

    @GetMapping
    public String test(@AuthenticationPrincipal User user) {
        if (user != null) {

            return "Hello " + user.getMail();
        }

        return "not connected";
    }

    @GetMapping("/account")
    public User getAccount(@AuthenticationPrincipal User user) {
        return user;
    }
    
    public UserRepo getRepo() {
        return repo;
    }

    public void setRepo(UserRepo repo) {
        this.repo = repo;
    }
}
