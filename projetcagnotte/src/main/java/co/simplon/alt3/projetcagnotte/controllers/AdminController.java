package co.simplon.alt3.projetcagnotte.controllers;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt3.projetcagnotte.entities.Admin;
import co.simplon.alt3.projetcagnotte.repositories.AdminRepo ;
// import exception.SpecialCharacterException;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    private AdminRepo repo;    
    
    //FIND ALL
    @GetMapping
    public List<Admin> all() {
        return repo.findAll();
    }

    // @GetMapping("/{id}")
    // public List one(@PathVariable int id) {
    //     return repo.findById(id)
    //             .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    // }


    //FIND BY NAME
    @GetMapping ("/search/{name}")
    public List<Admin> searchByLabel(@PathVariable String name) {
        return repo.findByName(name);
    
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Admin add(@RequestBody @Validated Admin admin) {
        repo.save(admin);
        return admin;
    }

    // @DeleteMapping("/{id}")
    // @ResponseStatus(code = HttpStatus.NO_CONTENT)
    // public void delete(@PathVariable int id) {
    //     repo.delete(one(id));
    // }
    
    public AdminRepo getRepo() {
        return repo;
    }

    public void setRepo(AdminRepo repo) {
        this.repo = repo;
    }

}