package co.simplon.alt3.projetcagnotte.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import co.simplon.alt3.projetcagnotte.entities.Project;

@RepositoryRestResource(path = "admin")
public interface ProjectRepo extends JpaRepository <Project, Integer> {
    List<Project> findByName(String name);
}