package co.simplon.alt3.projetcagnotte.exceptions;

public class NameException extends Exception{
    public NameException(String m) {
        super(m);
    }
    public NameException() {
        super("Le nom est incorrecte !");
    }
}
