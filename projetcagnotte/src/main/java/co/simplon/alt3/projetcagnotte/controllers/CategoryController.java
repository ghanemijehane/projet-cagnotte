package co.simplon.alt3.projetcagnotte.controllers;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.projetcagnotte.entities.Category;
import co.simplon.alt3.projetcagnotte.repositories.*;
// import exception.SpecialCharacterException;

@Controller
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryRepo repo;    
    
    //FIND ALL
    @GetMapping
    public List<Category> all() {
        return repo.findAll();
    }

    //FIND BY NAME
    // @GetMapping ("/search/{label}")
    // public List<Category> searchByLabel(@PathVariable String label) {
    //     return repo.findByLabel(label);
    
    // }

    //FIND BY ID
    @GetMapping("/{id}")
    public Category one(@PathVariable int id) {
        return repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        ;
    }
    
    public CategoryRepo getRepo() {
        return repo;
    }

    public void setRepo(CategoryRepo repo) {
        this.repo = repo;
    }
}
