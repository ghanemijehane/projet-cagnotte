package co.simplon.alt3.projetcagnotte.exceptions;

public class WrongPasswordException extends Exception {
 public WrongPasswordException(String m )   {
    super(m);
 }
 public WrongPasswordException()   {
    super("Mot de passe incorrect");
 }
}
