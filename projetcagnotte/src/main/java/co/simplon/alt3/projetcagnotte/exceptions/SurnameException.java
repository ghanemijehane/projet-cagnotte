package co.simplon.alt3.projetcagnotte.exceptions;

public class SurnameException extends Exception{
    public SurnameException(String m) {
        super(m);
    }
    public SurnameException() {
        super("Le prenom est incorrecte !");
    }
}