package co.simplon.alt3.projetcagnotte.exceptions;

public class LongPriceException extends Exception {
    public LongPriceException(String m) {
        super(m);
    }
    public LongPriceException() {
        super("La somme est trop excessive !");
    }
}