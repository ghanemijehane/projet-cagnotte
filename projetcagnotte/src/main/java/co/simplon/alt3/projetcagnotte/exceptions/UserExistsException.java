package co.simplon.alt3.projetcagnotte.exceptions;


public class UserExistsException extends Exception {

    public UserExistsException() {
        super("User already exists");
    }

}