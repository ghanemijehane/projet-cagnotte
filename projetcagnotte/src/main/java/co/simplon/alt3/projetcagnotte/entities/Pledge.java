package co.simplon.alt3.projetcagnotte.entities;

import java.security.InvalidParameterException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="pledge")

public class Pledge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Project projet;
    private double amount;

    public Pledge(int id, User user, Project projet, double amount) {
        this.id = id;
        this.user = user;
        this.projet = projet;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProjet() {
        return projet;
    }

    public void setProjet(Project projet) {
        this.projet = projet;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {

        if(amount < 1 && amount > 20000 ) {
            throw new InvalidParameterException("Le montant est incorrecte et dois être compris entre 1 et 20000 !");
        }      
        this.amount = amount;
    }
 
}
