package co.simplon.alt3.projetcagnotte.auth;


public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
