package co.simplon.alt3.projetcagnotte.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import co.simplon.alt3.projetcagnotte.entities.User;

@RepositoryRestResource(path = "admin")
public interface UserRepo extends JpaRepository <User, Integer> {
    List<User> findByName(String name);
    Optional<User> findByMail (String mail);

}