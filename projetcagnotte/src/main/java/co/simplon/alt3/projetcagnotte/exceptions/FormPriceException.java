package co.simplon.alt3.projetcagnotte.exceptions;

public class FormPriceException extends Exception {
    public FormPriceException(String m) {
        super(m);
    }
    public FormPriceException() {
        super("La forme du prix est incorecte !");
    }
}