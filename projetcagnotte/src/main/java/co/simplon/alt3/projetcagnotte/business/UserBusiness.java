package co.simplon.alt3.projetcagnotte.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import co.simplon.alt3.projetcagnotte.auth.ChangePasswordDto;
import co.simplon.alt3.projetcagnotte.auth.UserRoles;
import co.simplon.alt3.projetcagnotte.entities.User;
import co.simplon.alt3.projetcagnotte.exceptions.UserExistsException;
import co.simplon.alt3.projetcagnotte.exceptions.WrongPasswordException;
import co.simplon.alt3.projetcagnotte.repositories.UserRepo;


@Service
public class UserBusiness {
    
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PasswordEncoder encoder;

    public User register(User user) throws UserExistsException {

        if(userRepo.findByMail(user.getMail()).isPresent()){
            throw new UserExistsException();
        }
        user.setId(null);
        user.setRole(UserRoles.ROLE_USER.name());
        hashPassword(user);

        userRepo.save(user);
        
        connectUser(user);
        

        return user;
    }

    public void changePassword(ChangePasswordDto passwordDto, User user) throws WrongPasswordException {
        if(!encoder.matches(passwordDto.oldPassword, user.getPassword())) {
            throw new WrongPasswordException();
        }
        user.setPassword(passwordDto.newPassword);
        hashPassword(user);
        
        userRepo.save(user);
    }

    private void connectUser(User user) {
        
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
    }

    private void hashPassword(User user) {
        String hashed = encoder.encode(user.getPassword());
        user.setPassword(hashed);
    }
}