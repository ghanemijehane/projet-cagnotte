package co.simplon.alt3.projetcagnotte.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import co.simplon.alt3.projetcagnotte.entities.Project;
import co.simplon.alt3.projetcagnotte.repositories.ProjectRepo ;
// import exception.SpecialCharacterException;

@Controller
@RequestMapping("/api/project")
public class ProjectController {
    @Autowired
    private ProjectRepo repo;    
    
    //FIND ALL
    @GetMapping
    public List<Project> all() {
        return repo.findAll();
    }

    // @GetMapping("/{id}")
    // public List one(@PathVariable int id) {
    //     return repo.findById(id)
    //             .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    // }


    //FIND BY NAME
    @GetMapping ("/search/{name}")
    public List<Project> searchByLabel(@PathVariable String name) {
        return repo.findByName(name);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Project add(@RequestBody @Validated Project project) {
        repo.save(project);
        return project;
    }

    // @DeleteMapping("/{id}")
    // @ResponseStatus(code = HttpStatus.NO_CONTENT)
    // public void delete(@PathVariable int id) {
    //     repo.delete(one(id));
    // }
    
    public ProjectRepo getRepo() {
        return repo;
    }

    public void setRepo(ProjectRepo repo) {
        this.repo = repo;
    }

}
