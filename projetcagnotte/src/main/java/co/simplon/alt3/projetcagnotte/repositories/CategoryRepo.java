package co.simplon.alt3.projetcagnotte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import co.simplon.alt3.projetcagnotte.entities.Category;

@RepositoryRestResource(path = "admin")
public interface CategoryRepo extends JpaRepository <Category, Integer> {
    List<Category> findByLabel(String label);
}