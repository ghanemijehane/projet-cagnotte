package co.simplon.alt3.projetcagnotte.entities;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="project")

public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private Date dateCreation;
    private Date dateEnd;
    private String image;
    private Double totalAmount;
    private Double minAmount;
    private Boolean contribution;
    private String labelContribution;
    @ManyToOne
    private User owner;
    @ManyToOne
    private Category category;
    
    public Project(Integer id, String name, String description, Date dateCreation, Date dateEnd, String image, Double totalAmount,
            Double minAmount, Boolean contribution, String labelContribution, User owner, Category category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateCreation = dateCreation;
        this.dateEnd = dateEnd;
        this.image = image;
        this.totalAmount = totalAmount;
        this.minAmount = minAmount;
        this.contribution = contribution;
        this.labelContribution = labelContribution;
        this.owner = owner;
        this.category = category;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    public Boolean getContribution() {
        return contribution;
    }

    public void setContribution(Boolean contribution) {
        this.contribution = contribution;
    }

    public String getLabelContribution() {
        return labelContribution;
    }

    public void setLabelContribution(String labelContribution) {
        this.labelContribution = labelContribution;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    
}
