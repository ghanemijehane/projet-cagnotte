package co.simplon.alt3.projetcagnotte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import co.simplon.alt3.projetcagnotte.entities.Admin;

@RepositoryRestResource(path = "admin")
public interface AdminRepo extends JpaRepository <Admin, Integer> {
    List<Admin> findByName(String name);
}
